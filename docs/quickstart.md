# Quick Start Guide
Get started quickly with IICS using below steps.

1. You will need a UW-Madison NetID in order to access the IICS web console (known as the designer).
2. Login into the IICS web console:
    * [test](https://dm-us.informaticacloud.com/ma/sso/fu0Dw88PzqRdTYPctT73QJ)[^1]
    * [prod](https://dm-us.informaticacloud.com/ma/sso/732dcgB8WwTgRubL1mFU8R)

If you can't access IICS web console reach out to us: ais-enterprise-integration-informatica@office365.wisc.edu  

## Where To Go Next?
Learn more about IICS [concepts](./concepts.md), [tutorials](./tutorials.md), and [training](./training.md), or 
get started using Informatica get started [guide](https://network.informatica.com/docs/DOC-17653). 

[^1]: __Test__ IICS uses the NetID Login QA environment, which is connected to production credentials.
