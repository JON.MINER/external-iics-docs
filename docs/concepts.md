# IICS Concepts

### Secure Agent
A Java program that runs all tasks and enables secure communication across the firewall between our organization and 
IICS. More details can be found [here](https://docs.informatica.com/integration-cloud/cloud-platform/current-version/administrator/runtime-environments/secure-agents.html).